var express = require('express');

const clienteRouter = require("./clientes");
const app = express();

app.disable("x-powered-by");
app.use(`/clientes`, clienteRouter);
module.exports = app;
