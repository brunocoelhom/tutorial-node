const express = require("express");
const router = express.Router();
const ClienteController = require("../controllers/ClienteController.js")
const client = new ClienteController();

router.post(
    "/",
   client.create
  );
  router.put(
    "/:id",
   client.update
  );
  router.get(
    "/",
   client.list
  );
  router.get(
    "/:id",
  client.search
  );
  router.delete(
    "/:id",
   client.delete
  );
  module.exports = router;