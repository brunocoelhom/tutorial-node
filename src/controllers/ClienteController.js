const apiResponse = require("../helpers/apiResponse");
const Cliente = require("../models/clientes");

class ClienteController {
  async create(req, res) {
    try {
      const data = await Cliente.create(req.body);
      return apiResponse.successResponseWithData(
        res,
        "Cliente created with Success.",
        data
      );
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  }

  async list(req, res) {
    try {
      const data = await Cliente.find({ isDeleted: false });
      return apiResponse.successResponseWithData(
        res,
        "Operation success",
        data
      );
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  }

  async search(req, res) {
    try {
      const data = await Cliente.find({
        isDeleted: false,
        _Id: req.params.id,
      });
      return apiResponse.successResponseWithData(
        res,
        "Operation success",
        data
      );
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  }

  async delete(req, res) {
    try {
      Cliente.findById(req.params.id, function (err, found) {
        if (found === null) {
          return apiResponse.notFoundResponse(
            res,
            "Cliente not exists with this id " + req.params.id
          );
        } else {
          Cliente.findByIdAndUpdate(
            { _id: req.params.id },
            { isDeleted: true },
            {},
            function (err2) {
              if (err2) {
                return apiResponse.ErrorResponse(res, err2);
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "Cliente deleted Success."
                );
              }
            }
          );
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  }
  async update(req, res) {
    try {
      Cliente.findById(req.params.id, function (err, found) {
        if (found === null) {
          return apiResponse.notFoundResponse(
            res,
            "Cliente not exists with this id " + req.params.id
          );
        } else {
          const object = new Cliente(req.body);
          object._id = req.params.id;
          Cliente.findByIdAndUpdate(
            { _id: req.params.id },
            object,
            {},
            function (err2) {
              if (err2) {
                return apiResponse.ErrorResponse(res, err2);
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "Cliente update Success."
                );
              }
            }
          );
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  }
}

module.exports = ClienteController;