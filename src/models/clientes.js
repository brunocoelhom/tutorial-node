const mongoose = require("mongoose")
const Schema = mongoose.Schema

const ClienteSchema = new Schema(
    {
        nome:{
            type:String,
            required:true
        },
        email:{
            type:String,
            required:true
        },
        telefone:{
            type:Number,
            required:true
        },
        
    },
    { collection: "clientes" }
);
const Clientes = mongoose.model("clientes", ClienteSchema, "clientes");

module.exports = Clientes;